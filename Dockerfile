FROM ubuntu:14.04
RUN rm /bin/sh && ln -snf /bin/bash /bin/sh

MAINTAINER sreenivas "nukalasr@bitbucket.org"

# Update aptitude with new repo
RUN apt-get -yqq update

# Install software
RUN apt-get install -yqq git
RUN apt-get install -yqq net-tools
# Make ssh dir

FROM python:2.7
ENV PYTHONBUFFERED 1
RUN easy_install pip

ADD repo-key /
RUN chmod 600 /repo-key
RUN echo "IdentityFile /repo-key" >> /etc/ssh/ssh_config
RUN echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config

# Create known_hosts

# Clone the conf files into the docker container
RUN git clone git@bitbucket.org:mythdoor/mythdoor-web.git /myth
RUN cd /myth
RUN pip install virtualenv
RUN virtualenv venv-univtop
RUN /bin/bash -c "source venv-univtop/bin/activate"
RUN apt-get -yqq update
RUN apt-get -yqq install libxml2-dev libxslt1-dev python-dev zlib1g-dev libssl-dev libmemcached-dev
RUN   pip install -r /myth/reqs/dev.txt \
    && pip install -r /myth/reqs/common.txt

RUN pip install django celery django-celery django-allauth South django-haystack django-app-metrics geopy django-redis-cache
ENV PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.7/
RUN mkdir -p /myth/mythdoor/logs/
RUN mkdir -p /myth/mythdoor/db/dev
RUN python /myth/manage.py syncdb --noinput
RUN python /myth/manage.py migrate djcelery --noinput&& python /myth/manage.py migrate main --noinput
RUN python /myth/manage.py migrate
RUN cd /myth && python build.py dev --noinput
RUN apt-get install -y redis-server
RUN mkdir -p /var/log/supervisor
RUN apt-get install -y supervisor
#RUN pip install supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# clean packages
RUN apt-get clean
RUN rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/*

EXPOSE 22 80 443 8000 6379

#CMD ["/usr/bin/supervisord"]
ENTRYPOINT ["service", "supervisor", "restart"]
#ENTRYPOINT ["python", "/myth/manage.py", "runserver", "0.0.0.0:8000"]