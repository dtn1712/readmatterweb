var page_type = getPageType();

$(function() {
	$("#alert_message button.close").click(function() {
		$("#alert_message").slideUp();
	})
})

$(function() {
  	$('[data-toggle="popover"]').popover()
})


$(function() {
	window.addEventListener("dragover",function(e){
	  	e = e || event;
	  	if (e.target.tagName != "INPUT") { // check wich element is our target
	    	e.preventDefault();
	  	}

	},false);
	window.addEventListener("drop",function(e){
	 	e = e || event;
	  	if (e.target.tagName != "INPUT") { // check wich element is our target
	    	e.preventDefault();
	  	}
	},false);
})

$(function() {
	triggerLazyLoadImage();
})
