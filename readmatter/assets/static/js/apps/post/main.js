$(function() {
	if (page_type == 'post_create' || page_type == "post_update") {
		$("#id_category").change(function() {
			var category = $(this).val()
			$(".additional-detail-section").addClass("hidden");
			if (category == "housing") {
				$("#housing_detail_section").removeClass("hidden");
			}
			if (category == 'personal') {
				$("#price_field").addClass("hidden");
			} else {
				$("#price_field").removeClass("hidden");
			}

			$(".sub-category-select").val("");
			var subcategory_el = "#" + category + "_subcategory";
			if (isExist(subcategory_el)) {
				$(".sub-category-select").addClass("hidden");
				$(".sub-category-select").removeAttr("required");
				$(subcategory_el).removeClass("hidden");
				$(subcategory_el).attr("required","required");
				$("#subcategory_field").removeClass("hidden");
			} else {
				$("#subcategory_field").addClass("hidden");
			}
		})

		$(".sub-category-select").change(function() {
			$("#id_subcategory").val($(this).val());
			var category = $("#id_category").val();
			var subcategory = $(this).val();
			$(".additional-detail-section").addClass("hidden");
			if (category == "auto" && subcategory == "car") {
				$("#auto_car_detail_section").removeClass("hidden");
			}
			if (category == "housing") {
				$("#housing_detail_section").removeClass("hidden");
			}
		})

		$("#available_on_datepicker").on("change.bfhdatepicker",function() {
			$("input[name='available_on']").val($(this).val());
		})
	}
})


$(function() {
	if (page_type == 'post_create' || page_type == "post_update") {

		$("input[name='contact_by']").change(function() {
			$("input[name='contact_permission']").val($("input[name='contact_by']:checked").val());
		});
	}
})

$(function() {

	if (page_type == 'post_create' || page_type == "post_update") {
		$.validator.addMethod("atLeastOne",function(value,element) {
			var contact_by = $("input[name='contact_by']:checked").val();

			var is_error = false;
			if (contact_by == "specified_professional") {
				var companies = $("#id_companies").val();
				if (companies.length == 0) is_error = true;
			}
			return !is_error;
		},"You have to enter at least one company name");

		$("form.post-form").validate({
			ignore: ":hidden:not('#id_contact_by_companies')",
			rules: {
				contact_by: {
					required: true,
				},
				contact_by_companies: {
					atLeastOne: true,
				},
				postal_code: {
					required: true,
					zipcodeUS: true
				},
				price: {
					required: false,
					number: true,
					min: 0
				},
				mileage: {
					required: false,
					number: true,
					min: 0
				},
				year: {
					required:false,
					number:true,
					min:1960,
					max:2016,
				}
			}
		});

		$("form.post-form").submit(function(e) {
			var photos = $("#id_photos").val();
			if (photos.length > 0) {
				$("#id_photos").val(photos.substring(0,photos.length-1));
			}
			var contact_by_companies = $("#id_contact_by_companies").val();
			if (contact_by_companies.length > 0){
				var result = "";
				var companies_list = contact_by_companies.substring(0,contact_by_companies.length-1).split(";");
				for (var i = 0; i < companies_list.length; i++) {
					var company_data = companies_list[i].split(":");
					result = result + company_data[0] + ";";
				}
				$("#id_contact_by_companies").val(result.substring(0,result.length-1));
			}

			return true;
		})
	}
})

$(function() {

	if (page_type == 'post_create' || page_type == "post_update") {
		var angle = 0;
		$("#image_rotate_left_btn").on("click",function() {
			angle = angle - 90;
			var parent_el = $("#image_preview").parent();
			if (Math.abs(angle) % 360 == 90 || Math.abs(angle) % 360 == 270) {
				$(parent_el).css("padding-top","40px");
				$(parent_el).css("padding-bottom","40px");
			} else {
				$(parent_el).css("padding-top","0px");
				$(parent_el).css("padding-bottom","0px");
			}
			$("#image_preview").css("transform","rotate(" + angle + "deg)");
			$("#upload_photo_rotation_angle").val(angle);
		})
		$("#image_rotate_right_btn").on("click",function() {
			angle = angle + 90;
			$("#image_preview").css("transform","rotate(" + angle + "deg)");
			var parent_el = $("#image_preview").parent();
			if (Math.abs(angle) % 360 == 90 || Math.abs(angle) % 360 == 270) {
				$(parent_el).css("padding-top","40px");
				$(parent_el).css("padding-bottom","40px");
			} else {
				$(parent_el).css("padding-top","0px");
				$(parent_el).css("padding-bottom","0px");
			};
			$("#upload_photo_rotation_angle").val(angle);
		})
	}
});


$(function() {

	if (page_type == 'post_create' || page_type == "post_update") {

		var file_container = document.getElementById("upload_photo_container")
		var file_input = document.getElementById("upload_photo_input")

		file_input.addEventListener("change",function(e){
			//handleUploadPhoto(this.files)
			previewImage(file_input);
			$("#image_preview_modal").modal("show");
		},false)

		file_input.addEventListener("click",function(e){
			this.value = null;
		},false)

		file_container.addEventListener("click",function(e){
			$(file_input).show().focus().click().hide();
			e.preventDefault();
		},false)

		file_input.addEventListener("dragenter",function(e){
			e.stopPropagation();
			e.preventDefault();
		},false);

		file_input.addEventListener("dragover",function(e){
			e.stopPropagation();
			e.preventDefault();
		},false);

		file_input.addEventListener("drop",function(e){
			e.stopPropagation();
			e.preventDefault();
			previewImage(file_input);
			//handleUploadPhoto(e.dataTransfer.files);
		},false);

		$("#upload_photo_final_btn").click(function() {
			handleUploadPhoto(file_input.files);
		})
	}
})


$(function(){

	if (page_type == 'post_create' || page_type == "post_update") {
		var contact_by_companies_tag = $("#id_companies");

		var company_data = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			prefetch: {
			  url:"/api/v1/autocomplete/company/all/?isList=1",
			  ttl: 0,
			}
		});

		company_data.initialize();

		contact_by_companies_tag.tagsinput({
			itemValue: "value",
			itemText: "label",
		})

		contact_by_companies_tag.tagsinput('input').typeahead(null, {
			displayKey: "label",
			source: company_data.ttAdapter(),
			templates: {
				suggestion: Handlebars.compile([
					'<div><p>{{label}}</p></div>'
				].join(''))
			}
		}).bind('typeahead:selected', $.proxy(function (obj, datum) {
			this.tagsinput('input').typeahead('val', '');
			var contact_by_companies = $("#id_contact_by_companies").val();
			if (contact_by_companies.indexOf(datum['value']) == -1) {
				$("input[name='contact_by']").prop("checked",false);
				$("#contact_by_specified_professional").prop("checked",true);
				$("#id_contact_by_companies").val(contact_by_companies + datum['value'] + ":" + datum['label'] + ";")
				this.tagsinput("add",datum['label']);
			}
		}, contact_by_companies_tag));

		contact_by_companies_tag.on("beforeItemAdd",function(event) {
			hideAlertMessage();
			if ($("#id_contact_by_companies").val().indexOf(event.item) == -1) {
				showAjaxLoadingIcon();
				ajaxGet("/ajax/company/check_company_valid",{"company_name":event.item},function(content) {
					checkCompanyValidCallback(content);
				})
			}
		})

		contact_by_companies_tag.on("beforeItemRemove",function(event) {
			var contact_by_companies = $("#id_contact_by_companies").val();
			var result = "";
			var companies_list = contact_by_companies.substring(0,contact_by_companies.length-1).split(";");
			for (var i = 0; i < companies_list.length; i++) {
				var company_data = companies_list[i].split(":");
				if (event.item != company_data[1]) {
					result = result + company_data[0] + ":" + company_data[1] + ";";
				}
			}
			$("#id_contact_by_companies").val(result);
		})
	}
})

$(function() {
	if (page_type == 'post_detail') {

		google.maps.visualRefresh = true;

		geocoder = new google.maps.Geocoder();

		var map_options = {
			zoom: 14,
			scrollwheel: true,
			zoomControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		}
		var detail_post_map = initializeMap("detail_post_map_canvas",map_options);

		var postal_code = $("#post_postal_code").val();
		var housing_address = setEmptyForUndefined($("#post_housing_detail_address").val());
		var address, isIncludeMarker,isShowAddress;
		if (housing_address.trim().length == 0) {
			address = postal_code;
			isIncludeMarker = false;
			isShowAddress = false;
		} else {
			address = housing_address + " " + postal_code;
			isIncludeMarker = true;
			isShowAddress = true;
		}
		geocoder.geocode({'address': address},function(results,status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var location = results[0].geometry.location;
				detail_post_map.setCenter(location);
				if (isShowAddress) {
					$("#post_address").text(results[0].formatted_address);
				}
				if (isIncludeMarker) {
					var marker = new google.maps.Marker({
			            map: detail_post_map,
			            position: location
			        });
				}
		        $("#post_google_map_link").attr("href","https://www.google.com/maps?q=loc:" + location);
			} else {
				$("#post_address").text(address);
			}
		})
	}
})

$(function() {
	if (page_type == 'post_detail') {
		$(".post-photo-thumbnail").hover(function() {
			var item_unique_id = $(this).attr("data-unique-id");
			var src  = $(".post-photo-display-item[data-unique-id='" + item_unique_id + "']").attr("src");
			$("#post_display_photo").attr("src",src);
		});
	}
})

$(function() {
	if (page_type == "post_detail") {
		$("#handle_watch_post_btn").click(function(){
			var post_unique_id = $("#post_unique_id").val();
			var value = $(this).attr("data-value");
			if (value == "not_watching") {
				showAjaxLoadingIcon();
				ajaxPost("/ajax/post/watch_post",{"post_unique_id": post_unique_id},function(content) {
					watchPostCallback(content);
				})
			} else if ( value == "watching") {
				showAjaxLoadingIcon();
				ajaxPost("/ajax/post/unwatch_post",{"post_unique_id": post_unique_id},function(content) {
					unwatchPostCallback(content);
				})
			}
		});

		$("#handle_watch_post_btn").hover(
			function() {
				var value = $(this).attr("data-value");
				if (value == "watching") {
					$(this).html("Unwatch")
				}
			},
			function() {
				var value = $(this).attr("data-value");
				if (value == "watching") {
					$(this).html('<span class="glyphicon glyphicon-ok mini-margin-right" aria-hidden="true"></span>Watching');
				}
			}
		)
	}
})

$(function() {
	if (page_type == "post_detail") {
		$("#send_message_final_btn").click(function() {
			var message_content = $("#contact_post_input").val();
			if (stripWhitespace(message_content).length != 0 ) {
				var post_unique_id = $("#post_unique_id").val();
				var user_chat_username = $("#post_user_create_username").val();
				var data = {
					"post_unique_id": post_unique_id,
					"user_chat_username": user_chat_username,
					"message_content": message_content
				}
				showAjaxLoadingIcon();
				ajaxPost("/ajax/message/create_conversation",data, function(content){
					createConversationCallback(content);
				})
			}
		})
	}
})

$(function() {
	if (page_type == "post_detail") {

		var post_unique_id = $("#post_unique_id").val();

		$("#sold_post_btn").click(function() {
			showAjaxLoadingIcon();
			ajaxPost("/ajax/post/check_post_interest_buyers",{"post_unique_id": post_unique_id},function(content){
				checkPostInterestBuyersCallback(content);
			})
		})

		$("#proceed_transaction_feedback_btn").click(function() {
			$("#complete_post_modal").modal("hide");
			$("#transaction_feedback_modal").modal({backdrop:"static"});

			showAjaxLoadingIcon();
			ajaxGet("/ajax/post/get_transaction_feedback_template",{"post_unique_id": post_unique_id},function(content) {
				getTransactionFeedbackTemplateCallback(content);
			})

		})

		$("#report_post_input").on("change keyup paste",function(){
			var value = $(this).val();
			if (value.trim().length > 0) {
				console.log("yes");
				$("#report_post_final_btn").removeAttr("disabled");
			} else {
				$("#report_post_final_btn").attr("disabled","disabled");
			}
		})

		$("#report_post_final_btn").click(function() {
			var report_content = $("#report_post_input").val();
			if (report_content.trim().length > 0) {
				showAjaxLoadingIcon();
				ajaxPost("/ajax/post/report_post",{"post_unique_id": post_unique_id, "report_content": report_content},function(content){
					reportPostCallback(content);
				})
			} else {
				$("#report_post_final_btn").attr("disabled","disabled");
			}
		})
	}
})
