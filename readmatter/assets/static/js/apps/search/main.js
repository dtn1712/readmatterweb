var page_type = getPageType();

$(function() {
	if (page_type == "search_post") {
		$("#id_category").change(function() {
			var value = $(this).val();
			$(".sub-category-select").addClass("hidden");
			$("#" + value + "_sub_category").removeClass('hidden');
			$(".additional-filter").addClass("hidden");
			if (value == "auto") {
				var sub_category_value = $("#" + value + "_sub_category").val();
				if (sub_category_value == "car") {
					$("#auto_filter_section").removeClass("hidden");
				}
			} else if (value == "housing") {
				$("#housing_filter_section").removeClass("hidden");
			}
		})

		$("#auto_sub_category").change(function() {
			var value = $(this).val();
			$(".additional-filter").addClass("hidden");
			if (value == "car") {
				$("#auto_filter_section").removeClass("hidden");
			}
		})
	}

})

$(function() {

	if (page_type == "search_post") {

		$("#search_form").submit(function(e) {
			var subcategory_selects = $(".sub-category-select");
			var subcategory_value = "";
			for (var i = 0; i < subcategory_selects.length; i++) {
				var subcategory = subcategory_selects[i];
				if ($(subcategory).hasClass("hidden") == false) {
					subcategory_value = $(subcategory).val();
				}
			}

			$("input[name='subcategory']").val(subcategory_value);

			return true;
		})
	}
})

$(function(){

	$("#search_location").change(function() {
		if ($(this).val().trim().length == 0) {
			$("input[name='lat']").val("");
			$("input[name='lng']").val("");
			$("input[name='location']").val("");
		}
	})
})

$(function() {
	$("#toggle_filter_search_btn").click(function(){
		var $item = $("#search_filter_section .panel-body");
		if ($item.hasClass("page-normal")) {
			$item.removeClass("page-normal");
			$item.addClass("page-responsive");
			$(this).html("Hide");
		} else if ($item.hasClass("page-responsive")) {
			$item.removeClass("page-responsive");
			$item.addClass("page-normal");
			$(this).html("Show");
		}
	})
})
