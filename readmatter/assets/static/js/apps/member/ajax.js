function updateSettingsCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var settings = $(data['settings_snippet'])
		$("#dashboard_settings").html(settings);
		$("#dashboard_settings input").change(function() {
			validateSettings();
		})
		showAlertMessage("Your settings has been updated successfully","success");
	} else {
		showAlertMessage("Failed to update preferred email. Please try again","danger",true);
	}
}

function sendConnectionRequestCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var is_approve_user_valid = data['is_approve_user_valid']
		$("dashboard_account_credential .error").addClass("hidden");
		if (is_approve_user_valid) {
			showAlertMessage("You have sent connection request successfully","success");
		} else {
			hideAlertMessage();
			$("#connection_email_error_ineligible").removeClass("hidden");
		}
	} else {
		showAlertMessage("Failed to send connection request. Please try again","danger",true);
	}
}

function approveConnectionRequestActionCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var connection_requests = $(data['connection_requests_snippet'])
		$("#connection_requests_section .panel-body").html(connection_requests);
		showAlertMessage("You have approved the connection request successfully","success")
	} else {
		showAlertMessage("Failed to approve the connection request. Please try again","danger",true);
	}
}

function rejectConnectionRequestActionCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var connection_requests = $(data['connection_requests_snippet'])
		$("#connection_requests_section .panel-body").html(connection_requests);
		showAlertMessage("You have rejected the connection request successfully","success")
	} else {
		showAlertMessage("Failed to reject the connection request. Please try again","danger",true);
	}
}

function updateOccupationCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var account_info = $(data['account_info_snippet'])
		$("#account_info_section").html(account_info);
	} else {
		showAlertMessage("Failed to update the occupation. Please try again","danger",true);
	}
}

function getUserPostsByStatusCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var user_posts = $(data['user_posts_snippet'])
		$("#listing_posts_section .panel-body").html(user_posts);
		$("#filterStatusDropdown").html(data['post_status']);
		triggerLazyLoadImage();
		triggerHolderImage();
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function sendBuyingPostMessageCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#buying_post_message_history").append(data['message_snippet']);
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function sendSellingPostMessageCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#selling_post_message_history").append(data['message_snippet']);
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function loadSellingConversationHistoryCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#selling_post_message_history").html(data['conversation_snippet']);
		$("#send_selling_post_message_btn").removeAttr("disabled");
		var post = data['conversation_post'];
		var post_link = "<a href='/post/" + post['unique_id'] + "'>" + post['title'] + "</a>";
		$("#selling_section_title").html(post_link);
		$("#selling_section_action").removeClass("hidden");
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function loadBuyingConversationHistoryCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#buying_post_message_history").html(data['conversation_snippet']);
		$("#send_buying_post_message_btn").removeAttr("disabled");
		var post = data['conversation_post'];
		var post_link = "<a href='/post/" + post['unique_id'] + "'>" + post['title'] + "</a>";
		$("#buying_section_title").html(post_link);
		$("#buying_section_action").removeClass("hidden");
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function deleteSellingConversationHistoryCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#selling_post_message_history").html("");
		$("#dashboard_selling li.post-conversation-item[data-unique-id='" + data['conversation_unique_id'] + "']").remove();
		$("#dashboard_selling li.list-group-item").removeClass("active");
		showAlertMessage("You have deleted your conversation successfully","success");
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function deleteBuyingConversationHistoryCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#buying_post_message_history").html("");
		$("#dashboard_buying li.conversation-item[data-unique-id='" + data['conversation_unique_id'] + "']").remove();
		showAlertMessage("You have deleted your conversation successfully","success");
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function clearNotificationCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var related_link = data['related_link'].trim();
		if (related_link.length != 0) {
			window.location.href = data['related_link'];
		} else {
			$(".notification-item[data-unique-id='" + data['notification_unique_id'] + "']").removeClass("new");
		}
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function clearAllNotificationsCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#dashboard_notification li.notification-item").removeClass("new");
		$(".new-notifications-count").html("");
	} else {
		showAlertMessage("An error has occurred during your request. Please try again","danger",true);
	}
}

function reportConversationCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		showAlertMessage("Thanks for your report. We will investigate on the issue as soon as possible","success");
	} else {
		showAlertMessage("An error occured when processing your request. Please try again","danger",true);
	}
}
