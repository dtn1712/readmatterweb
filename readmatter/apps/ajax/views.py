import logging

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django_ajax.decorators import ajax
from validate_email import validate_email

from readmatter.apps.ajax.helper import get_request_params, validate_request
from readmatter.apps.app_helper import generate_html_snippet, get_user_login_object
from readmatter.apps.main.constants import NEW
from readmatter.apps.main.constants import OLD, EMPTY_STRING
from readmatter.apps.main.models import Photo, Notification

logger = logging.getLogger(__name__)


@ajax
@login_required
def delete_upload_photo(request):
    result = {}
    try:
        validate_request(request, ['POST'])
        data = get_request_params(request, ['unique_id'])
        photo = Photo.objects.get(unique_id=data['unique_id'])
        photo.delete()
        result['success'] = True
        result['unique_id'] = data['unique_id']
    except Exception as e:
        logger.exception(e)
        result['success'] = False
        result['error_message'] = str(e)
    return result


@ajax
@login_required
def clear_notification(request):
    result = {}
    try:
        validate_request(request, ['POST'])
        data = get_request_params(request, ['notification_unique_id'])
        notification = Notification.objects.get(unique_id=data['notification_unique_id'])
        notification.status = OLD
        notification.save()
        result['notification_unique_id'] = data['notification_unique_id']
        result['related_link'] = notification.related_link
        result['success'] = True
    except Exception as e:
        logger.exception(e)
        result['success'] = False
        result['error_message'] = str(e)
    return result


@ajax
@login_required
def clear_all_notifications(request):
    result = {}
    try:
        validate_request(request, ['POST'])
        user_login = get_user_login_object(request)
        Notification.objects.filter(notify_to=user_login, status=NEW).update(status=OLD)
        result['success'] = True
    except Exception as e:
        logger.exception(e)
        result['success'] = False
        result['error_message'] = str(e)
    return result


@ajax
@login_required
def update_settings(request):
    result = {}
    try:
        validate_request(request, ['POST'])
        data = get_request_params(request, ["initial"], ['preferred_email'])
        initial = data['initial'].upper()
        preferred_email = EMPTY_STRING
        if "preferred_email" in data:
            preferred_email = data['preferred_email']

        user_login = get_user_login_object(request)
        user_login.userprofile.initial = initial
        if validate_email(preferred_email) or len(preferred_email) == 0:
            user_login.userprofile.preferred_email = preferred_email
            result['settings_snippet'] = generate_html_snippet(request, "user_settings",
                                                               {"user": user_login, "is_preferred_email_valid": True})
            result['success'] = True
        else:
            result['settings_snippet'] = generate_html_snippet(request, "user_settings",
                                                               {"user": user_login, "is_preferred_email_valid": False})
            result['success'] = False
        user_login.userprofile.save()
    except Exception as e:
        logger.exception(e)
        result['success'] = False
        result['error_message'] = str(e)
    return result


@ajax
@login_required
def check_exist_email(request):
    result = {}
    try:
        validate_request(request, ['GET'])
        data = get_request_params(request, ['email'])
        email = data['email']
        users = User.objects.filter(Q(email=email) | Q(userprofile__preferred_email=email))
        if len(users) == 0:
            result['is_exist'] = False
        else:
            user = users[0]
            user_login = get_user_login_object(request)
            if user_login == user:
                result['is_exist'] = False
            else:
                result['is_exist'] = True
        result['success'] = True
    except Exception as e:
        logger.exception(e)
        result['success'] = False
        result['error_message'] = str(e)
    return result


@ajax
@login_required
def update_occupation(request):
    result = {}
    try:
        validate_request(request, ['POST'])
        data = get_request_params(request, ['occupation'])
        occupation = data['occupation']
        user_login = get_user_login_object(request)
        user_login.userprofile.credential.credential_additional_info = occupation
        user_login.userprofile.credential.save()
        result['account_info_snippet'] = generate_html_snippet(request, "professional_account_info",
                                                               {"user": user_login})
        result['success'] = True
    except Exception as e:
        logger.exception(e)
        result['success'] = False
        result['error_message'] = str(e)
    return result
