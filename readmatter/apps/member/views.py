import logging

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from readmatter.apps.app_helper import get_user_login_object
from readmatter.apps.app_views import AppBaseView

logger = logging.getLogger(__name__)

APP_NAME = "member"

class BaseDashboardView(AppBaseView,DetailView):
	app_name = APP_NAME
	model = User
	queryset = None

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(BaseDashboardView, self).dispatch(*args, **kwargs)

	def get_object(self, queryset=None):
		return get_user_login_object(self.request)


class PostDashboardView(BaseDashboardView):
	template_name = "post"


class MessageDashboardView(BaseDashboardView):
	template_name = "message"


class AccountCredentialDashboardView(BaseDashboardView):
	template_name = "account_credential"


class SettingsDashboardView(BaseDashboardView):
	template_name = "settings"


class NotificationDashboardView(BaseDashboardView):
	template_name = "notification"

