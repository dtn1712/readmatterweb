from django.conf.urls import *

from readmatter.apps.search.views import PostSearchView

urlpatterns = [
    url(r'^', PostSearchView.as_view(), name='post_search'),
]
