import datetime
import logging
import os
from sys import path

from django.core.cache import cache

from readmatter.apps.app_settings import LOGO_ICON_URL

logger = logging.getLogger(__name__)

def build_user_autocomplete_data(users,container):
	for user in users:
		try:
			data = {
				"value": user.username,
				"label": user.userprofile.get_user_fullname(),
				"type": "People",
				"url": "/people/" + user.username
			}
			if user.userprofile.avatar != None:
				data['picture'] = user.userprofile.avatar.image.url
			else:
				data['picture'] = LOGO_ICON_URL
			container.append(data)
		except Exception as e:
			logger.exception(e)
	return container

def update_search_index():
	from readmatter.settings import ROOT_PATH
	path.append(ROOT_PATH)
	try:
		cmd = "python manage.py update_index"
		os.system(cmd)
		cache.set("last_index_updated",datetime.datetime.now())
	except Exception as e:
		logger.exception(e)
