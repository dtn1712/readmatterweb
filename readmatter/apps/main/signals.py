from django.contrib.auth.models import User
from django.core.cache import cache
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from tastypie.models import create_api_key

from readmatter.apps.main.models import UserProfile


@receiver(post_delete)
@receiver(post_save)
def clear_template_cache(sender, instance, **kwargs):
    list_of_models = ('User', 'UserProfile', 'Post', 'Notification',
                      "Message", "Conversation", "UserCredential", "UserConnectionRequest")
    if sender.__name__ in list_of_models:
        cache.delete_pattern("views.decorators.cache.*")


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


def signals_import():
    post_save.connect(create_api_key, sender=User)


signals_import()
