import logging

from django.apps import AppConfig
from django.core.cache import cache
from django.utils import timezone
from readmatter.apps.company.helper import get_company_common_info, generate_company_cache_key

logger = logging.getLogger(__name__)

class MainConfig(AppConfig):
	name = 'readmatter.apps.main'

	def init_cache_data(self):
		try:
			cache.clear()

			Company = self.get_model("Company")

			companies = Company.objects.all()
			companies_data = {}

			for company in companies:
				company_info = get_company_common_info(company)
				cache.set(generate_company_cache_key(company),company_info,timeout=None)
				companies_data[company.unique_id] = company_info

			cache.set_many({
				"companies": companies_data,
			},timeout=None)

			cache.set_many({
				"companies_last_cache_updated": timezone.now(),
			},timeout=None)

			cache.set("last_cache_updated",timezone.now(),timeout=None)



		except Exception as e:
			logger.exception(e)


	def ready(self):

        self.init_cache_data()


