from readmatter.apps.app_settings import SITE_DATA, DEFAULT_LATITUDE
from readmatter.apps.app_settings import DEFAULT_LONGITUDE, DEFAULT_CITY
from readmatter.apps.app_settings import DEFAULT_COUNTRY, DEFAULT_COUNTRY_CODE
from readmatter.apps.app_settings import DEFAULT_POSTAL_CODE

from readmatter.apps.main.constants import TRUE, FALSE, AUTO_COLOR_TYPE, AUTO_TITLE_STATUS
from readmatter.apps.main.constants import AUTO_TRANSMISSION, AUTO_BODY_TYPE, AUTO_CYLINDER_TYPE
from readmatter.apps.main.constants import AUTO_FUEL_TYPE, HOUSING_NUM_BEDROOM, HOUSING_NUM_BATHROOM
from readmatter.apps.main.constants import HOUSING_TYPE, NEW, OLD, PENDING_STATUS, ACTIVE_STATUS, EXPIRED_STATUS

from readmatter.apps.app_helper import get_client_ip
from readmatter.apps.app_helper import get_base_template_path

from readmatter.settings import STAGE, geoip


def site_data(request):
    return SITE_DATA


def global_data(request):
    results = {}
    client_ip = get_client_ip(request)
    results['client_ip'] = client_ip
    results['current_lat'] = DEFAULT_LATITUDE
    results['current_lng'] = DEFAULT_LONGITUDE
    results['current_city'] = DEFAULT_CITY
    results['current_country'] = DEFAULT_COUNTRY
    results['current_country_code'] = DEFAULT_COUNTRY_CODE
    results['current_postal_code'] = DEFAULT_POSTAL_CODE
    results['geo_data'] = None
    if geoip.city(client_ip) is not None:
        geo_data = geoip.city(client_ip)
        if geo_data['latitude'] is not None:
            results['current_lat'] = geo_data['latitude']
        if geo_data['longitude'] is not None:
            results['current_lng'] = geo_data['longitude']
        if geo_data['city'] is not None: results['current_city'] = geo_data['city']
        if geo_data['country_name'] is not None:
            results['current_country'] = geo_data['country_name']
        if geo_data['country_code'] is not None:
            results['current_country_code'] = geo_data['country_code']
        if geo_data['postal_code'] is not None:
            results['current_postal_code'] = geo_data['postal_code']
        results['geo_data'] = geo_data

    results['stage'] = STAGE
    results['base_template'] = get_base_template_path()
    return results


def constants_data(request):
    results = {
        'PENDING_STATUS': PENDING_STATUS,
        'ACTIVE_STATUS': ACTIVE_STATUS,
        'EXPIRED_STATUS': EXPIRED_STATUS,
        'NEW': NEW,
        'OLD': OLD,
        'TRUE': TRUE,
        'FALSE': FALSE,
        'AUTO_COLOR_TYPE': AUTO_COLOR_TYPE,
        'AUTO_TITLE_STATUS': AUTO_TITLE_STATUS,
        'AUTO_BODY_TYPE': AUTO_BODY_TYPE,
        'AUTO_FUEL_TYPE': AUTO_FUEL_TYPE,
        'AUTO_CYLINDER_TYPE': AUTO_CYLINDER_TYPE,
        'AUTO_TRANSMISSION': AUTO_TRANSMISSION,
        'HOUSING_TYPE': HOUSING_TYPE,
        'HOUSING_NUM_BEDROOM': HOUSING_NUM_BEDROOM,
        'HOUSING_NUM_BATHROOM': HOUSING_NUM_BATHROOM,
    }
    return results
