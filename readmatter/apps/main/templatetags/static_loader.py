import jinja2
import logging

from django_jinja import library

from readmatter.apps.app_helper import read_catalogue
from readmatter.settings import BUILD_VERSION_ID, DEFAULT_APP_NAME
from readmatter.settings import ROOT_PATH, STATIC_URL, SITE_NAME

logger = logging.getLogger(__name__)


@library.global_function
@jinja2.contextfunction
def load_js(context):
    stage = context['stage']
    app_name = DEFAULT_APP_NAME if "app_name" not in context else context['app_name']
    result = ''
    if stage == "dev":
        plugin_files, global_files = [], []
        read_catalogue(plugin_files, ROOT_PATH + "/assets/static/js/plugins/", None)
        read_catalogue(global_files, ROOT_PATH + "/assets/static/js/global/", None)
        for filename in plugin_files:
            result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/plugins/' + filename + '"></script>\n'

        for filename in global_files:
            result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/global/' + filename + '"></script>\n'

        result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/apps/' + app_name + '/ajax.js"></script>\n' + \
                 '<script type="text/javascript" src="' + STATIC_URL + 'js/apps/' + app_name + '/function.js"></script>\n' + \
                 '<script type="text/javascript" src="' + STATIC_URL + 'js/apps/' + app_name + '/main.js"></script>\n'
    else:
        result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/prod/' + SITE_NAME + ".script." + app_name + "." + BUILD_VERSION_ID + '.min.js"></script>'

    return result


@library.global_function
@jinja2.contextfunction
def load_css(context):
    stage = context['stage']
    app_name = DEFAULT_APP_NAME if "app_name" not in context else context['app_name']
    result = ""
    if stage == "dev":
        css_path = ROOT_PATH + "/assets/static/css/"
        list_file = []
        read_catalogue(list_file, css_path, None)
        for filename in list_file:
            result = result + '<link rel="stylesheet" href="' + STATIC_URL + 'css/' + filename + '" type="text/css" />\n'
        result = result + '<link rel="stylesheet" href="' + STATIC_URL + 'css/apps/' + app_name + '.css" type="text/css" />'
        return result
    else:
        return '<link rel="stylesheet" href="' + STATIC_URL + 'css/prod/stylesheets/' + app_name + '.' + BUILD_VERSION_ID + '.min.css" type="text/css" />'
