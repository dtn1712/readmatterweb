import datetime
import logging

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from imagekit.models import ProcessedImageField

from readmatter.apps.account.settings import UNIQUE_EMAIL, EMAIL_CONFIRMATION_EXPIRE_DAYS
from readmatter.apps.app_helper import generate_unique_id
from readmatter.apps.main.constants import MESSAGE_STATUS, NEW, DEFAULT_POST_QUOTA
from readmatter.apps.main.constants import NOTIFICATION_STATUS, NOTIFICATION_TYPE
from readmatter.apps.main.constants import PASSCODE, ACTIVATE_LINK
from readmatter.apps.main.constants import USER_EMAIL_TYPE
from readmatter.apps.main.managers import EmailAddressManager, EmailConfirmationManager

logger = logging.getLogger(__name__)

try:
	storage = settings.MULTI_IMAGES_FOLDER + '/'
except AttributeError:
	storage = 'upload_storage/'


def get_unique_id():
	return generate_unique_id()


class Email(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	to_emails = models.TextField()
	email_template = models.CharField(max_length=200)
	subject = models.CharField(max_length=350,blank=True)
	text_content = models.TextField(blank=True)
	context_data = models.TextField()
	status = models.CharField(max_length=50,default="pending")
	send_time = models.DateTimeField(blank=True,null=True)
	request_time = models.DateTimeField(auto_now_add=True)


class Photo(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	name = models.CharField(max_length=150,blank=True)
	image = ProcessedImageField(upload_to=storage+"/photo/%Y/%m/%d",format='JPEG',options={'quality': 70},blank=True,null=True)
	rotation_angle = models.IntegerField(default=0)
	image_url = models.CharField(max_length=300,blank=True)
	image_secure_url = models.CharField(max_length=300,blank=True)
	width = models.IntegerField()
	height = models.IntegerField()
	generated_aliases = models.CharField(max_length=300,blank=True)
	is_all_aliases_generated = models.BooleanField(default=False)
	upload_time = models.DateTimeField(auto_now_add=True)
	upload_success = models.BooleanField(default=True)
	user_create = models.ForeignKey(User,blank=True,null=True,on_delete=models.SET_NULL)

	def __unicode__(self):
		return self.name

class Message(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	user_send = models.ForeignKey(User,related_name="user_send")
	user_receive = models.ForeignKey(User,related_name="user_receive")
	content = models.TextField()
	status = models.CharField(max_length=1,choices=MESSAGE_STATUS,default=NEW)
	create_time = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.content

class Notification(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	content = models.TextField()
	status = models.CharField(max_length=1,choices=NOTIFICATION_STATUS,default=NEW)
	notification_type = models.CharField(max_length=30,choices=NOTIFICATION_TYPE,null=True)
	notify_to = models.ForeignKey(User, related_name='notify_to')
	notify_from = models.ForeignKey(User, related_name='notify_from',blank=True,null=True,on_delete=models.SET_NULL)
	related_link = models.TextField(blank=True)
	create_time = models.DateTimeField(auto_now_add=True)


class UserProfile(models.Model):
	user = models.OneToOneField(User)

	email_type = models.CharField(max_length=20,choices=USER_EMAIL_TYPE)
	credential = models.ForeignKey("UserCredential",related_name="user_profile_credential",blank=True,null=True,on_delete=models.SET_NULL)
	initial = models.CharField(max_length=2)
	preferred_email = models.EmailField(blank=True,null=True)
	post_quota = models.IntegerField(default=DEFAULT_POST_QUOTA)
	watching_posts = models.ManyToManyField(Post,related_name="user_watching_posts",blank=True)

	def __unicode__(self):
		return unicode(self.user)




@python_2_unicode_compatible
class EmailAddress(models.Model):

	user = models.ForeignKey(User,verbose_name=_('main_user'),related_name="user_email_address")
	email = models.EmailField(unique=True,verbose_name=_('e-mail address'))
	verified = models.BooleanField(verbose_name=_('verified'), default=False)
	primary = models.BooleanField(verbose_name=_('primary'), default=False)

	objects = EmailAddressManager()

	class Meta:
		verbose_name = _("email address")
		verbose_name_plural = _("email addresses")
		if not UNIQUE_EMAIL:
			unique_together = [("user", "email")]

	def __str__(self):
		return u"%s (%s)" % (self.email, self.user)

	def set_as_primary(self, conditional=False):
		old_primary = EmailAddress.objects.get_primary(self.user)
		if old_primary:
			if conditional:
				return False
			old_primary.primary = False
			old_primary.save()
		self.primary = True
		self.save()
		user_email(self.user, self.email)
		self.user.save()
		return True

	def send_confirmation(self, request, confirm_type, signup=False):
		confirmation = None
		try:
			confirmation = EmailConfirmation.objects.get(email_address=self)
		except EmailConfirmation.DoesNotExist:
			confirmation = EmailConfirmation.create(self)

		confirmation.send(request,confirm_type,signup=signup)
		return confirmation

	def change(self, request, new_email, confirm=True):
		"""
		Given a new email address, change self and re-confirm.
		"""
		with transaction.commit_on_success():
			user_email(self.user, new_email)
			self.user.save()
			self.email = new_email
			self.verified = False
			self.save()
			if confirm:
				self.send_confirmation(request)


@python_2_unicode_compatible
class EmailConfirmation(models.Model):

	email_address = models.ForeignKey(EmailAddress,verbose_name=_('e-mail address'))
	created = models.DateTimeField(verbose_name=_('created'),default=timezone.now)
	sent = models.DateTimeField(verbose_name=_('sent'), null=True)
	email_sent = models.ForeignKey(Email,related_name="email_sent_confirmation",blank=True,null=True,on_delete=models.SET_NULL)
	key = models.CharField(verbose_name=_('key'), max_length=64, unique=True)
	passcode = models.CharField(verbose_name=_('passcode'), max_length=5)

	objects = EmailConfirmationManager()

	class Meta:
		verbose_name = _("email confirmation")
		verbose_name_plural = _("email confirmations")

	def __str__(self):
		return u"confirmation for %s" % self.email_address

	@classmethod
	def create(cls, email_address):
		key = random_token([email_address.email])
		passcode = str(random.randrange(10000, 99999, 3))
		return cls._default_manager.create(email_address=email_address,
										   key=key,passcode=passcode)

	def key_expired(self):
		expiration_date = self.sent \
			+ datetime.timedelta(days=EMAIL_CONFIRMATION_EXPIRE_DAYS)
		return expiration_date <= timezone.now()
	key_expired.boolean = True

	def confirm(self, request):
		if not self.key_expired() and not self.email_address.verified:
			email_address = self.email_address
			email_address.verified = True
			email_address.set_as_primary(conditional=True)
			email_address.save()
			signals.email_confirmed.send(sender=self.__class__,
										 request=request,
										 email_address=email_address)
			return email_address

	def send(self, request, confirm_type, signup=False, **kwargs):
		context = {}
		email_template = None
		if confirm_type == PASSCODE:
			context_data = { "passcode": self.passcode }
			email_template = "confirmation_passcode_signup" if signup else "confirmation_passcode"
		elif confirm_type == ACTIVATE_LINK:
			activate_url = "/api/v1/auth/activate?confirm_type=activate_link&value=" + self.key + "?format=json"
			activate_url = request.build_absolute_uri(activate_url)
			context_data = {"activate_url": activate_url }
			email_template = "confirmation_link_signup" if signup else "confirmation_link"
		else:
			raise Exception(message=("Incorrect confirm type. Please choose either {passcode} or {activate_link}".format(passcode=PASSCODE,activate_link=ACTIVATE_LINK)))

		email = Email.objects.create(to_emails=self.email_address.email,email_template=email_template,context_data=str(context_data))

		self.email_sent = email
		self.sent = timezone.now()
		self.save()
