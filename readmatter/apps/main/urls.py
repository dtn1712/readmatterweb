from django.conf.urls import *

from readmatter.apps.main.views import MainView

urlpatterns = [
    url(r"^$", MainView.as_view(),name='home'),
]
