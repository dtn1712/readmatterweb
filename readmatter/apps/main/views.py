import logging

from django.http import HttpResponse
from django.template import engines
from django.views.generic import ListView

from readmatter.apps.app_views import AppBaseView

logger = logging.getLogger(__name__)

APP_NAME = "main"


class MainView(AppBaseView, ListView):
    app_name = APP_NAME
    template_name = "index"

    def get_queryset(self):
        return []

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        return context


def handler404(request):
    data = {}
    jinja_engine = engines['jinja']
    template = jinja_engine.get_template('sites/404.jinja.html')
    return HttpResponse(template.render(context=data, request=request))


def handler500(request):
    data = {}
    jinja_engine = engines['jinja']
    template = jinja_engine.get_template('sites/500.jinja.html')
    return HttpResponse(template.render(context=data, request=request))
