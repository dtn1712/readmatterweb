from celery import shared_task

from readmatter.apps.app_helper import data_backup as data_backup_helper


@shared_task
def data_backup():
    data_backup_helper()
