from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from readmatter.apps.api.autocomplete import AutocompleteResource
from readmatter.apps.api.email import EmailResource
from tastypie.api import Api

from readmatter import settings

#from djrill import DjrillAdminSite

#admin.site = DjrillAdminSite()
admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(AutocompleteResource())
v1_api.register(EmailResource())

urlpatterns = [
    # Admin URL
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/defender/', include('defender.urls')), # defender admin

    # Auth
    url(r'^account/', include("readmatter.apps.account.urls")),

    # Haystack app URL
    url(r'^search/', include("readmatter.apps.search.urls")),

    url(r'^ajax/', include("readmatter.apps.ajax.urls")),

    # API
    url(r'^api/', include(v1_api.urls)),

    # readmatter URL
    url(r'^$', include('readmatter.apps.main.urls')),
    url(r'^user/',include('readmatter.apps.member.urls')),
    url(r'^post/',include('readmatter.apps.post.urls')),
    url(r'^photo/',include('readmatter.apps.photo.urls')),
    url(r'^about/',include('readmatter.apps.about.urls')),

    url(r'^i18n/', include('django.conf.urls.i18n')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = "readmatter.apps.main.views.handler404"
handler500 = "readmatter.apps.main.views.handler500"

